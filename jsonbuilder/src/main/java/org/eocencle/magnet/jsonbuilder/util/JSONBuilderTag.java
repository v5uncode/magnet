package org.eocencle.magnet.jsonbuilder.util;

/**
 * Xml建构标记类
 * @author: huan
 * @Date: 2020-05-31
 * @Description:
 */
public class JSONBuilderTag {
    public static final String JSON_ATTR_PARAMETER          = "parameter";
    public static final String JSON_ATTR_DATASOURCE	        = "datasource";
    public static final String JSON_ATTR_WORKFLOW		    = "workflow";
    public static final String JSON_ATTR_KEY		        = "key";
    public static final String JSON_ATTR_VALUE		        = "value";
    public static final String JSON_ATTR_LIST		        = "list";
    public static final String JSON_ATTR_MAP		        = "map";
    public static final String JSON_ATTR_TYPE		        = "type";
    public static final String JSON_ATTR_ID		            = "id";
    public static final String JSON_ATTR_ALIAS		        = "alias";
    public static final String JSON_ATTR_STYLE		        = "style";
    public static final String JSON_ATTR_SRC		        = "src";
    public static final String JSON_ATTR_REF		        = "ref";
    public static final String JSON_ATTR_FORMAT		        = "format";
    public static final String JSON_ATTR_SEPARATOR		    = "separator";
    public static final String JSON_ATTR_TOPICS		        = "topics";
    public static final String JSON_ATTR_CONFIG		        = "config";
    public static final String JSON_ATTR_FIELDS		        = "fields";
    public static final String JSON_ATTR_NAME		        = "name";
    public static final String JSON_ATTR_PRECISION		    = "precision";

    public static final String DATASOURCE_TYPE_TABLE		= "table";
    public static final String DATASOURCE_TYPE_STREAM		= "stream";

    public static final String TABLE_STYLE_DEFAULT  = "default";
    public static final String TABLE_STYLE_FILE     = "file";
    public static final String TABLE_STYLE_HTTP     = "http";
    public static final String TABLE_STYLE_DATABASE = "database";
    public static final String TABLE_STYLE_KAFKA    = "kafka";

}
