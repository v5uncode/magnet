# Magnet

#### 介绍
三分钟快速搭建流式处理应用！简单实用的分布式大数据处理框架，特点是零基础操作，支持批处理和流式处理。

<img src="https://img.shields.io/badge/Build-Java8-red.svg?style=flat" />
<img src="https://img.shields.io/badge/Licence-Apache2.0-green.svg?style=flat" />
<a href='https://gitee.com/huanStephen/magnet/stargazers'><img src='https://gitee.com/huanStephen/magnet/badge/star.svg?theme=dark' alt='star'></img></a>
<a href='https://gitee.com/huanStephen/magnet/members'><img src='https://gitee.com/huanStephen/magnet/badge/fork.svg?theme=dark' alt='fork'></img></a>

#### 软件架构
![Magnet架构图](https://images.gitee.com/uploads/images/2020/0429/233028_cc6696d7_612479.png "Magnet架构图.png")
项目目前由core、xmlbuilder、spark1和client四个模块组成，core模块封装了各个部分的抽象组件；xmlbuilder模块是以xml解析为执行配置来源的读取模块；spark1模块为大数据引擎模块；client模块是客户端调用模块。项目可以扩展执行配置的解析方式和大数据处理引擎，良好的接口可以兼容任何大数据引擎。架构图中蓝色部分为框架的核心模块，非蓝色部分均为可扩展模块。你可以扩展配置文件的格式，可以是json配置、db配置或自定义格式；你也可以扩展大数据处理引擎，Flink、Hive、Kafka或MapReduce；你也可以扩展各种标签组件；你还可以扩展每个标签组件的包装wrapper，用于处理每个标签组件的前置和后置操作。

#### 使用说明
- [Magnet介绍](https://gitee.com/huanStephen/magnet/wikis/pages?sort_id=2060770&doc_id=611470)
- [快速入门](https://gitee.com/huanStephen/magnet/wikis/pages?sort_id=2065930&doc_id=611470)
- [配置文件结构](https://gitee.com/huanStephen/magnet/wikis/pages?sort_id=2065943&doc_id=611470)
- [使用IDEA打包项目](https://gitee.com/huanStephen/magnet/wikis/pages?sort_id=2242934&doc_id=611470)
- [在yarn上运行Magnet](https://gitee.com/huanStephen/magnet/wikis/pages?sort_id=2290903&doc_id=611470)
- [parameter参数标签](https://gitee.com/huanStephen/magnet/wikis/pages?sort_id=2065993&doc_id=611470)
- [fragment碎片标签](https://gitee.com/huanStephen/magnet/wikis/pages?sort_id=2067961&doc_id=611470)
- [datasource数据源标签](https://gitee.com/huanStephen/magnet/wikis/pages?sort_id=2068240&doc_id=611470)
- [workflow工作流标签](https://gitee.com/huanStephen/magnet/wikis/pages?sort_id=2076458&doc_id=611470)
> - [SQL标签](https://gitee.com/huanStephen/magnet/wikis/pages?sort_id=2076458&doc_id=611470)
> - [filter标签](https://gitee.com/huanStephen/magnet/wikis/pages?sort_id=2081461&doc_id=611470)
> - [distinct标签](https://gitee.com/huanStephen/magnet/wikis/pages?sort_id=2152335&doc_id=611470)
> - [output标签](https://gitee.com/huanStephen/magnet/wikis/pages?sort_id=2083864&doc_id=611470)
> - [valueMappers标签](https://gitee.com/huanStephen/magnet/wikis/pages?sort_id=2108139&doc_id=611470)
> - [splitFieldToRows标签](https://gitee.com/huanStephen/magnet/wikis/pages?sort_id=2141081&doc_id=611470)
> - [stringCuts标签](https://gitee.com/huanStephen/magnet/wikis/pages?sort_id=2148552&doc_id=611470)
> - [addFields标签](https://gitee.com/huanStephen/magnet/wikis/pages)
> - [addSequence标签](https://gitee.com/huanStephen/magnet/wikis/pages)
- [开发指南](https://gitee.com/huanStephen/magnet/wikis/pages?sort_id=2129207&doc_id=611470)
> - [框架结构介绍](https://gitee.com/huanStephen/magnet/wikis/pages?sort_id=2129207&doc_id=611470)
> - [扩展新处理引擎](https://gitee.com/huanStephen/magnet/wikis/pages?sort_id=2129391&doc_id=611470)
> - [扩展新标签](https://gitee.com/huanStephen/magnet/wikis/pages?sort_id=2160799&doc_id=611470)

#### 参与贡献
[贡献者列表](https://gitee.com/huanStephen/magnet/contributors?ref=master)

#### 加入我们
如果：

- 你对Magnet感兴趣
- 你有风骚的创意
- 你对大数据技术有兴趣
- 你想用Magnet解决工作中的问题，却发现Magnet解决不了
- 你想加入Magnet认识更多帅哥
- 未完待续...

你都可以加入我们Magnet社区，我们为你提供创造性的舞台接受你风骚的想法，或者解决实际工作中繁杂的任务。

<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=06ecb7c18b0f2eec1114a35694602caf690b4ad5b071f83a47bd902162e84ef1"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="Magnet交流社区" title="Magnet交流社区"></a>

#### 项目演示
示例测试文件链接: https://pan.baidu.com/s/1HSQLHD5mzDhlDe4FgQamuw 提取码: e9ip

批处理示例：

![批处理](https://images.gitee.com/uploads/images/2020/0331/214445_a5d64ad6_612479.gif "magnet-03311.gif")

流处理示例：

![流处理](https://images.gitee.com/uploads/images/2020/0514/230321_2f122ee2_612479.gif "magnet-stream.gif")